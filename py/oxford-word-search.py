#!/usr/bin/env python3

'''
    Finds meaningful words from Oxfod Dictionary 
    Solve scrambled words by permutations
'''

from itertools import permutations
import requests as rq
import sys

app_id = 'd54314dc'
app_key = '75e4b944e81c9a9d0d05455d43554896'
url = 'https://od-api.oxforddictionaries.com:443/api/v2/entries/en-us/{0}?fields=definitions&strictMatch=true'
headers = {'app_id' : app_id, 'app_key' : app_key}

def word_search(word):
    resp = rq.get(url.format(word), headers=headers).json()

    if 'error' not in resp:
        for i in resp['results']:
            for j in i['lexicalEntries']:
                for entry in j['entries']:
                    if 'senses' in entry:
                        for sense in entry['senses']:
                            for defn in sense['definitions']:
                                print(word + ' : ' + defn, end='\n\n')

                            for subsense in sense['subsenses']:
                                for subsensdefn in subsense['definitions']:
                                    print(word + ' : ' + subsensdefn, end='\n\n')

def permute_str(string):
    for perm in permutations(string):
        word_search(''.join(perm))
    
def error():
    print(
    '''\
Usage:  jumble-words.py <operation> <string>
    operation
        search  To search for meanings of a word
        solve   To permute a string for meaningful words\
    ''')

    sys.exit(0)

if len(sys.argv) < 3:
    error()

op = sys.argv[1]
string = sys.argv[2]

if op == 'search':
    word_search(string)

elif op == 'solve':
    permute_str(string)

else:
    error()

