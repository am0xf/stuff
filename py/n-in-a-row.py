#!/usr/bin/env python3

import sys

def make_grid():
	global grid
	grid = []

	for i in range(nor):
		grid.append(['.'] * nor)

def show_win(player):
    print('Player ' + player + ' wins')

def show_grid():
    print('*', end='\t')
    for i in range(nor):
        print(i+1, end='\t')
    print()

    for i in range(nor):
        print(i+1, end='\t')

        for j in grid[i]:
            print(j, end='\t')
        print()

    print()

def check_rows():
    for i in range(nor):
        for j in range(nor - iar + 1):
            if grid[i][j] != '.':
                for k in range(iar):
                    if grid[i][j+k] != grid[i][j]:
                        return

                show_win(grid[i][j])

                for k in range(iar):
                    grid[i][j+k] = '*'

                show_grid()

                sys.exit(0)

def check_cols():            
    for i in range(nor): # col no.
        for j in range(nor - iar + 1): # row no.

            if grid[j][i] != '.':
                for k in range(iar):
                    if grid[j+k][i] != grid[j][i]:
                        return

                show_win(grid[j][i])

                for k in range(iar):
                    grid[j+k][i] = '*'

                show_grid()

                sys.exit(0)

def check_backdiag():
    for i in range(nor-iar + 1):
        for j in range(iar-1, nor):
            if grid[i][j] != '.':
                for k in range(iar):
                    if grid[i+k][j-k] != grid[i][j]:
                        return

                show_win(grid[i][j])

                for k in range(iar):
                    grid[i+k][j-k] = '*'

                show_grid()

                sys.exit(0)

def check_forwdiag():
    for i in range(nor-iar + 1):
        for j in range(nor-iar + 1):
            if grid[i][j] != '.':
                for k in range(iar):
                    if grid[i+k][j+k] != grid[i][j]:
                        return

                show_win(grid[i][j])

                for k in range(iar):
                    grid[i+k][j+k] = '*'

                show_grid()

                sys.exit(0)

def check_win():
    check_rows()
    check_cols()
    check_backdiag()
    check_forwdiag()

def player_input(turn):
    while True:
        show_grid()
        sr, sc = input('Player ' + str(turn) + '\'s turn : ').split(',')
        r, c = int(sr) - 1, int(sc) - 1

        if r < nor - 1 and grid[r+1][c] == '.':
            print('Gravity duh.')
            continue

        if r >= nor or c >= nor or grid[r][c] != '.':
            print('Are you blind?')
            continue

        return r, c


def play():
    turn = -1
    moves = 0

    global grid

    while moves < 81:
        r, c = player_input(turn+2)
        grid[r][c] = chr(48 + turn + 2)
        check_win()
        turn = ~turn
        moves += 1

if __name__ == '__main__':
	global nor
	global iar 
	nor = int(input("Enter number of rows : "))
	iar = nor // 2
	make_grid()
	play()

