#!/usr/bin/env python3

import sys
import os

fl = open('games', 'r')

def make_grid(moves):
	grid = [[' '] * 3 for i in range(3)]

	p = 0
	for i in moves:
		grid[(int(i) - 1) // 3][(int(i) - 1) % 3] = 'X' if p == 0 else 'O'
		p = ~p

	return grid

def show_grid(moves):
	grid = make_grid(moves)

	print()
	print(' ' + grid[0][0] + ' | ' + grid[0][1] + ' | ' + grid[0][2])
	print('--- --- ---')
	print(' ' + grid[1][0] + ' | ' + grid[1][1] + ' | ' + grid[1][2])
	print('--- --- ---')
	print(' ' + grid[2][0] + ' | ' + grid[2][1] + ' | ' + grid[2][2])
	print()

def check_win(moves):
	grid = make_grid(moves)
	# check row
	for i in range(3):
		if grid[i][0] in ['X', 'O']:
			if grid[i][0] == grid[i][1] == grid[i][2]:
				return grid[i][0]

	# check col 
	for i in range(3):
		if grid[0][i] in ['X', 'O']:
			if grid[0][i] == grid[1][i] == grid[2][i]:
				return grid[0][i]

	# check fwd diag
	if grid[0][0] in ['X', 'O']:
		if grid[0][0] == grid[1][1] == grid[2][2]:
			return grid[0][0]

	# check bckwd diag
	if grid[2][0] in ['X', 'O']:
		if grid[2][0] == grid[1][1] == grid[0][2]:
			return grid[0][2]

	return 'No result'	

def gen_moves(moves, player, program, fl):
	for pos in set('123456789') - set(moves):
		tmp_mvs = moves + pos

		res = check_win(tmp_mvs)

		if res == 'No result':
			if len(tmp_mvs) == 9:
			# draw
				fl.write('D' + ' ' + tmp_mvs + '\n')

			else:
				gen_moves(tmp_mvs, 'X' if player == 'O' else 'O', program, fl)

		else:
			fl.write(res + ' ' + tmp_mvs + '\n')

def gen_all_games():
	fle = open('games_unsorted', 'w')
	gen_moves('', 'X', 'X', fle)
	fle.close()

def program_play(moves, program):
	global fl

	cur_pos1 = fl.tell()
	line = ''
	move = ''

	while True:
		cur_pos1 = fl.tell()
		line = fl.readline().split(' ')

		if line[0] not in [program, 'D']:
			# move to draw cases
			fl.seek(0)
			while fl.readline().split(' ')[0] != 'D':
				cur_pos1 = fl.tell()
			fl.seek(cur_pos1)

		# if move sequence starts with moves already played
		if line[1].startswith(moves):
			move = line[1][len(moves)]
			break
		
	fl.seek(cur_pos1)
	return move

def start():
	moves = ''

	user = input("X or O ? : ")
	program = 'X' if user == 'O' else 'O'

	cur_pos = 0

	#while not fl.readline().startswith(program):
		#cur_pos = fl.tell()

	#fl.seek(cur_pos)
	
	for cur in ['X', 'O'] * 4 + ['X']:
		move = ''

		#if cur == program:
			#move = program_play(moves, program)

		#else:
		show_grid(moves)
			
		while True:
			move = input("Enter position : ")
			if move not in moves:
				break
				
		moves += move

		if check_win(moves) == cur:
			show_grid(moves)
			print(cur + ' won')
			break

		else:
			if len(moves) == 9:
				show_grid(moves)
				print('Draw')
				break
	
	#fl.close()

def error():
	print("ttt.py [Generate]")

if __name__ == '__main__':
	np = len(sys.argv)
	if np == 2:
		if sys.argv[1] == 'Generate':
			gen_all_games()
			os.system('sort games_unsorted > games && rm games_unsorted')
			exit(0)
		
		else:
			error()

	elif np > 2:
		error()

	else:
		start()

