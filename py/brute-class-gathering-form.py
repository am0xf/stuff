#!/usr/bin/env python3

import requests as rq
import random

#url = 'https://docs.google.com/forms/d/e/1FAIpQLSevNPgGwwlbZTsjCMY77nghjPh2AUKZZ3YKfzd90AfABchBJg/formResponse'

headers = {
		'Referer' : 'https://docs.google.com/forms/d/e/1FAIpQLSevNPgGwwlbZTsjCMY77nghjPh2AUKZZ3YKfzd90AfABchBJg/viewform',
		'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0',
		'Host' : 'docs.google.com',
		'Origin' : 'https://docs.google.com'
		}


rollsa = [r for r in range(40, 52)]
namesa = ['Rohit', 'Arihant', 'Abhijeet', 'Shubham', 'Rahul', 'Shivani', 'Vamsi', 'Anviksha', 'Siddhant', 'Rachit', 'Utkarsh', 'Soumili']

rollsb = [r for r in range(55, 71)]
namesb = ['Akshit', 'Tanay', 'Aneet', 'Uday', 'Chintanalapani', 'Shresht', 'Riya', 'Amritansh', 'Ashwin', 'Aditya', 'Pratyush', 'Mrigank', 'Animesh', 'Atika', 'Deepshit', 'Pooja']

name_rolla = {
		'Kimberly' : '1', 
		'Ronnin' : '2', 
		'Jayesh' : '3', 
		'Kishore' : '4', 
		'Sharad' : '5', 
		'Radhika' : '9', 
		'Rohan' : '10' , 
		'Akshatha' : '11', 
		'Naman' : '13', 
		'Nalin' : '21'
		}

name_rollc = {
	'Sanmesh' : '72', 
	'Siva' : '73', 
	'Sanjan' : '75', 
	'Sushma' : '76'
	}

name_rollb = {
		'Sheen' : '25', 
		'Phanendra' : '26', 
		'Akhilesh' : '27', 
		'Utkarsh' : '28', 
		'Arnab' : '29', 
		'Shashank' : '30', 
		'Aman' : '33', 
		'Shirin' : '34', 
		'Nitin' : '35', 
		'Ritwick' : '36'
		}

class_lunch = ['Always+in', 'No']
class_party = ['Hell+yeah!', 'No']

while True:
	for name in name_rolla:
		roll = name_rolla[name]
		data = {'entry.1331308915': name, 'entry.1518078183': roll, 'entry.645508862': random.choice(class_lunch), 'entry.1768922636': random.choice(class_party), 'fvv': '1', 'draftResponse': '[null, null, "132389386246810888"]', 'pageHistory': '0', 'fbzx': '132389386246810888'}

		rq.post(url, data=data)

		print(name, roll)

	for name in name_rollb:
		roll = name_rollb[name]
		l = random.choice(class_lunch)
		p = random.choice(class_party)
		data = {'entry.1331308915': name, 'entry.1518078183': roll, 'entry.645508862': l, 'entry.1768922636': p, 'fvv': '1', 'draftResponse': '[null, null, "132389386246810888"]', 'pageHistory': '0', 'fbzx': '132389386246810888'}

		rq.post(url, data=data)

		print(name, roll)

	for (name, roll) in zip(namesa, rollsa):
		data = {'entry.1331308915': name, 'entry.1518078183': roll, 'entry.645508862': random.choice(class_lunch), 'entry.1768922636': random.choice(class_party), 'fvv': '1', 'draftResponse': '[null, null, "132389386246810888"]', 'pageHistory': '0', 'fbzx': '132389386246810888'}

		rq.post(url, data=data)

		print(name, roll)

	for (name, roll) in zip(namesb, rollsb):
		data = {'entry.1331308915': name, 'entry.1518078183': roll, 'entry.645508862': random.choice(class_lunch), 'entry.1768922636': random.choice(class_party), 'fvv': '1', 'draftResponse': '[null, null, "132389386246810888"]', 'pageHistory': '0', 'fbzx': '132389386246810888'}

		rq.post(url, data=data)

		print(name, roll)

	for name in name_rollc:
		roll = name_rollc[name]
		data = {'entry.1331308915': name, 'entry.1518078183': roll, 'entry.645508862': random.choice(class_lunch), 'entry.1768922636': random.choice(class_party), 'fvv': '1', 'draftResponse': '[null, null, "132389386246810888"]', 'pageHistory': '0', 'fbzx': '132389386246810888'}

		rq.post(url, data=data)

		print(name, roll)

