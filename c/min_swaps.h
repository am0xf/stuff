/*
   * Accept array of consecutive integers in the range [1, n], n <= UINT_MAX
   * Return total number of swaps needed to sort in ascending order
*/

unsigned int pow2(unsigned int i)
{
	int p = 1;
	while (i--)
		p *= 2;

	return p;
}

int min_swaps(int *arr, int n)
{
	int swaps = 0;

	unsigned int pos = 0; // position bit field

	// use 1 indexing from here on

	int i = 1; // index into the virtual array of visited elements
	int j;

	while (i < n) {
		pos |= pow2(i-1); // visit

		j = arr[i-1]; // go to index at which arr[i-1] should be at

		// now if i is already in correct place then j == arr[i-1] == i, so vis[i-1] == vis[j-1] == 1. so following loop is not executed

		while (!(pos & pow2(j-1))) { // we haven't visited j-1
			pos |= pow2(j-1); // visit

			j = arr[j-1]; // go to index at which arr[j-1] should be at

			swaps++;
		}

		while (pos & pow2(i-1)) i++; // find 1st 0 element in vis
	}

	return swaps;
}

